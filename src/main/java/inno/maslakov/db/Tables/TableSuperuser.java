package inno.maslakov.db.Tables;

import inno.maslakov.db.RequestManager;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 * Created by sa on 21.02.17.
 */
@XmlRootElement
public class TableSuperuser implements RequestManager {

    private Connection connection;
    @XmlElement
    private ArrayList<Superuser> superusers = new ArrayList<>(100);

    private TableSuperuser(){}

    public TableSuperuser(Connection connection) {
        this.connection = connection;
    }

    @Override
    public void selectTable() throws SQLException {
        Statement query = connection.createStatement();
        ResultSet resultSet = query.executeQuery("SELECT * from superuser");
        while (resultSet.next()){
            Superuser superuser = Superuser.setFields(resultSet);
            superusers.add(superuser);
        }
    }

    public ArrayList<Superuser> getSuperusers() {
        return superusers;
    }


    public static class Superuser{
        @XmlElement Long id;
        @XmlElement String firstname;
        @XmlElement String middlename;
        @XmlElement String lastname;
        @XmlElement String email;

        @Override
        public String toString() {
            return "id = " + id +
                    " firstname: " + firstname +
                    " middlename: " + middlename +
                    " lastname: " + lastname +
                    " email: " + email;
        }


        public static Superuser setFields (ResultSet resultSet) throws SQLException {
            Superuser superuser = new Superuser();
            superuser.id = (Long) resultSet.getObject("id");
            superuser.firstname = (String) resultSet.getObject("firstname");
            superuser.middlename = (String) resultSet.getObject("middlename");
            superuser.lastname = (String) resultSet.getObject("lastname");
            superuser.email = (String) resultSet.getObject("email");

            return superuser;
        }
    }
}
