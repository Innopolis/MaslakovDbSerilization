package inno.maslakov.db.Tables;

import inno.maslakov.db.RequestManager;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 * Created by sa on 21.02.17.
 */
@XmlRootElement
public class TableCallReason implements RequestManager {
    private Connection connection;
    private ArrayList<CallReason> callReasons = new ArrayList<>(100);

    public TableCallReason(Connection connection) {
        this.connection = connection;
    }

    @Override
    public void selectTable() throws SQLException {
        Statement query = connection.createStatement();
        ResultSet resultSet = query.executeQuery("SELECT * from call_reason");
        while (resultSet.next()){
            CallReason callReason = CallReason.setFields(resultSet);
            callReasons.add(callReason);
        }
    }

    public ArrayList<CallReason> getCallReasons() {
        return callReasons;
    }

    public static class CallReason{
        @XmlElement Long id;
        @XmlElement String name;
        @XmlElement String script;

        @Override
        public String toString() {
            return "id = " + id +
                    " name: " + name +
                    " script: " + script;
        }

        public static CallReason setFields(ResultSet resultSet) throws SQLException {
            CallReason callReason = new CallReason();
            callReason.id = (Long) resultSet.getObject("id");
            callReason.name = (String) resultSet.getObject("name");
            callReason.script = (String) resultSet.getObject("script");
            return callReason;
        }
    }
}
