package inno.maslakov.db.Tables;


import inno.maslakov.db.RequestManager;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.sql.*;
import java.util.*;
import java.util.Date;

/**
 * Created by sa on 21.02.17.
 */@XmlRootElement
public class TableUser implements RequestManager {
    private Connection connection;
    @XmlElement
    private ArrayList<User> users = new ArrayList<>(100);

    public TableUser(Connection connection) {
        this.connection = connection;
    }

    private TableUser(){}

    @Override
    public void selectTable() throws SQLException {
        Statement query = connection.createStatement();
        ResultSet resultSet = query.executeQuery("SELECT * from \"user\" ");
        while (resultSet.next()){
            User user = User.setFields(resultSet);
            users.add(user);
        }
    }

    public ArrayList<User> getUsers() {
        return users;
    }


    public static class User{
        @XmlElement Long id;
        @XmlElement Long bitrix_id;
        @XmlElement String firstname;
        @XmlElement String middlename;
        @XmlElement String lastname;
        @XmlElement String email;
        @XmlElement String phone;
        @XmlElement java.util.Date birthdate;

        @Override
        public String toString() {
            return "id = " + id +
                    " bitrix_id: " + bitrix_id +
                    " firstname: " + firstname +
                    " middlename: " + middlename +
                    " lastname: " + lastname +
                    " email: " + email +
                    " phone: " + phone;
        }

        public static User setFields (ResultSet resultSet) throws SQLException {
            User user = new User();
            user.id = (Long) resultSet.getObject("id");
            user.bitrix_id = (Long) resultSet.getObject("bitrix_id");
            user.firstname = (String) resultSet.getObject("firstname");
            user.middlename = (String) resultSet.getObject("middlename");
            user.lastname = (String) resultSet.getObject("lastname");
            user.email = (String) resultSet.getObject("email");
            user.phone = (String) resultSet.getObject("phone");
            user.birthdate = (Date) resultSet.getObject("birthdate");

            return user;
        }
    }
}
