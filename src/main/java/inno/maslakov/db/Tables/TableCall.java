package inno.maslakov.db.Tables;

import inno.maslakov.db.RequestManager;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.sql.*;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by sa on 21.02.17.
 */
@XmlRootElement
public class TableCall implements RequestManager {
    private Connection connection;
    @XmlElement
    ArrayList<Call> calls = new ArrayList<>(100);

    public TableCall(Connection connection) {
        this.connection = connection;
    }

    private TableCall(){}

    @Override
    public void selectTable() throws SQLException {
        Statement query = connection.createStatement();
        ResultSet resultSet = query.executeQuery("SELECT * FROM call\n" +
                "INNER JOIN superuser on call.superuser_id = superuser.id\n" +
                "INNER JOIN call_reason on call.call_reason_id = call_reason.id\n" +
                "INNER JOIN \"user\" ON \"user\".id = call.user_id");
        while (resultSet.next()){
            Call call = setFields(resultSet);
            calls.add(call);
        }
    }

    private Call setFields (ResultSet resultSet) throws SQLException {
        Call call = new Call();
        call.id = resultSet.getLong("id");
        call.callReason = TableCallReason.CallReason.setFields(resultSet);
        call.user = TableUser.User.setFields(resultSet);
        call.superuser = TableSuperuser.Superuser.setFields(resultSet);
        call.status = resultSet.getInt("status");
        call.createdAt = resultSet.getTimestamp("created_at");

        return call;
    }

    public ArrayList<Call> getCalls() {
        return calls;
    }

    public static class Call{
        @XmlElement Long id;
        @XmlElement TableCallReason.CallReason callReason;
        @XmlElement TableUser.User user;
        @XmlElement TableSuperuser.Superuser superuser;
        @XmlElement Timestamp createdAt;
        @XmlElement Integer status;

        @Override
        public String toString() {
            return "id = " + id +
                    " CallReason: " + callReason +
                    " user: " + user +
                    " superuser: " + superuser +
                    " createdAtL " + createdAt +
                    " status: " + status;
        }
    }
}
