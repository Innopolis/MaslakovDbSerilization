package inno.maslakov.db;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by sa on 21.02.17.
 */
public interface RequestManager {
    void selectTable() throws SQLException;
}
