package inno.maslakov.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Created by sa on 20.02.17.
 */
public class ConnectManager {
    private static volatile ConnectManager instance;
    private static String url;
    private static String login;
    private static String password;
    private static Connection connection;

    public ConnectManager(String url, String login, String password) throws SQLException {
        this.url = url;
        this.login = login;
        this.password = password;
        getInstance();
    }

    private ConnectManager() {

    }

    public static ConnectManager getInstance() throws SQLException {
        ConnectManager localInstance = instance;
        if (localInstance == null) {
            synchronized (ConnectManager.class) {
                localInstance = instance;
                if (localInstance == null) {
                    instance = localInstance = new ConnectManager();
                }
            }
        }
        connection = DriverManager.getConnection(url, login, password);
        return localInstance;
    }

    public static Connection getConnection(){
        return connection;
    }
}
