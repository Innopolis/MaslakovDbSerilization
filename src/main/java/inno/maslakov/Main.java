package inno.maslakov;
import com.sun.org.apache.xpath.internal.SourceTree;
import inno.maslakov.db.ConnectManager;
import inno.maslakov.db.RequestManager;
import inno.maslakov.db.Tables.TableCall;
import inno.maslakov.db.Tables.TableCallReason;
import inno.maslakov.db.Tables.TableSuperuser;
import inno.maslakov.db.Tables.TableUser;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.PropertyException;
import java.io.File;
import java.sql.*;
import java.util.ArrayList;

/**
 * Created by sa on 20.02.17.
 */
public class Main {

    public static void main(String[] args) throws ClassNotFoundException, SQLException {
        String url = "jdbc:postgresql://127.0.0.1:5432/stc";
        String login = "root";
        String password = "root";
        Class.forName("org.postgresql.Driver");
        ConnectManager connectManager = new ConnectManager(url, login, password);
        Connection connection = connectManager.getConnection();

        TableUser tableUser = new TableUser(connection);
        tableUser.selectTable();
        System.out.println(tableUser.getUsers());

        TableCallReason tableCallReason = new TableCallReason(connection);
        tableCallReason.selectTable();
        System.out.println(tableCallReason.getCallReasons());

        TableSuperuser tableSuperuser = new TableSuperuser(connection);
        tableSuperuser.selectTable();
        System.out.println(tableSuperuser.getSuperusers());

        TableCall tableCall = new TableCall(connection);
        tableCall.selectTable();
        System.out.println(tableCall.getCalls());

        try {

            File file = new File("file.xml");
            JAXBContext jaxbContext = JAXBContext.newInstance(TableCall.class);
            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

            // output pretty printed
            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

            jaxbMarshaller.marshal(tableCall, file);
            jaxbMarshaller.marshal(tableCall, System.out);

        } catch (JAXBException e) {
            e.printStackTrace();
        }

    }




}
